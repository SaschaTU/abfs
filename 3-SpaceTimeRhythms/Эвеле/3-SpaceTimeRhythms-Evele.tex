\documentclass[%
	11pt,twoside,%
]{article}

%%%%%%%% METADATA TO FILL IN %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% AUTHOR: First name then last name                                %%
%% HEADAUTHOR: Last name only (should be automated)                 %%
%% PAGE: Starting page number for compilation (should be automated) %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\author{Эдуард Эвеле}
\newcommand{\headauthor}{Эвеле}
\setcounter{page}{14}
\title{Исследование зависимости длины береговой линии от масштаба карты}
%%%%%%%% END OF METADATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\input{../../preamble}

\begin{document}
\maketitle

\section{Введение}
\label{sec:Intro}
Отдых у берега моря является одним из самых приятных способов провести отпуск. Во всем мире существует множество таких мест, но для жителей Европы удобнее всего добираться до курортов Средиземного моря. А задумывались ли мы когда-нибудь, любуясь заходящем солнцем на морском берегу, что линия соприкосновения двух стихий может стать предметом для целого исследования?

Точное измерение береговой линии, как и любой другой границы, необходимо прежде всего государствам, так как разногласия в длине границы могут привести к конфликтам между сопредельными странами. Наука также не может остаться в стороне от измерения таких природных объектов, как береговые линии. Целью исследования является поиск зависимости изменения получаемой длины береговой линии от выбранного масштаба карты. В качестве объекта для данного исследования была выбрана береговая линия Греческой Республики, включая как ее континентальную часть, так и принадлежащие ей острова. В качестве критерия выбора данного географического объекта стоит выделить сильную изрезанность берега, что позволит легче понять исследуемую зависимость.

Прежде чем непосредственно перейти к измерению реального объекта, необходимо было понять принцип поиска длины береговой линии, основанный на сравнении ее с некоторой меркой известной длины. В таблице~\ref{tab:1} представлено краткое описание этих измерений.

\begin{table}
	\centering
	\caption{\label{tab:1}Измерения с помощью циркуля}
	\begin{tabular}{p{1pc}p{10pc}p{10pc}p{10pc}}
		\hline
	 	№ & Объект & Технология & Результаты \\
	 	\hline
	 	1 & Окружность радиуса $R$ & Откладывание на окружности (береговой линии) с помощью циркуля отрезков, равных длине мерки, подсчет их количества и умножение на величину мерки. & При уменьшении мерки длина окружности стремиться к реальному значению ($2\pi R$), а при увеличении~— к периметру вписанного правильного треугольника (или четырехугольника) \\
	 	\hline
	 	2 & Береговая линия Великобритании & —\,»\,— & При уменьшении длины мерки длина береговой линии стремиться к истинной длине (парадокс \abbr{БЛ} Ричардсона) \\
	 	\hline
	\end{tabular}
\end{table}

Парадокс \abbr{БЛ} Ричардсона разрешил математик Мандельброт, доказавший, что береговая линия является фракталом, и ее длина не имеет конечной истинной величины. В ходе измерения длины береговой линии Великобритании нами были получены доказательства этой теории. Одним из доказательств является вычисленная фрактальная размерность Британии, равная 1,26. Данный результат говорит о том, что береговая линия является фракталом, так как размерность находится между размерностью линии ($D=1$) и плоскости ($D=2$). Учитывая результаты измерения длины береговой линии Великобритании, можно сделать вывод, что длина \abbr{БЛ} зависит от величины выбранной мерки и ее истинная длина неопределяема. Проведенные далее измерения дали ответ на главный вопрос исследования: Зависит ли при одинаковой мерке длина \abbr{БЛ} также от масштаба карты?

\section{Методика}
\label{sec:Method}
Исследование главного объекта~— береговой линии Греции проводилось с использованием вычислительных мощностей персонального компьютера. Измерения проводились по следующим этапам:
\begin{enumerate}
	\item Сохранение в формате \abbr{PNG} карты Греции с помощью программы \textit{Maperetive}.
	\item Обработка изображения в графическом редакторе \textit{\abbr{GIMP\,2}}:
	\begin{enumerate}
		\item Расчет масштаба (измеряется в километрах на пиксель).
		\item Удаление ненужных пометок на карте (названия городов, линии торговых путей и т.д.).
		\item Обводка контура карты, удаление остального содержимого изображения.
		\item Удаление части контура, не относящегося к береговой линии Греции (озера, БЛ других государств.
		\item Заливка фона белым цветом.
	\end{enumerate}
	\item Анализ контура береговой линии в программе \textit{Fractalyse} (получение длины береговой линии для разных величин мерки).
	\item Занесение и обработка данных в электронной таблице \textit{Excel}:
	\begin{enumerate}
		\item Расчет длины в километрах для каждой мерки (умножение длины мерки  в пикселях на текущий масштаб)
		\item Нахождение значений \abbr{БЛ} (умножение длины каждой мерки на ее количество на карте по данным программы \textit{Fractalyse}).
		\item Занесение значений в таблицу, построение графика зависимости длины \abbr{БЛ} от длины мерки в километрах.
		\item Линеаризация графика путем построения его в двойных логарифмических координатах, получение уравнения графика вида $y=-dx+B$ , определение фрактальной размерности по формуле $D=d+1$ (выведение данной формулы на примере периметра острова Коха представлено в приложении в конце данной статьи).
	\end{enumerate}
	\item Увеличение масштаба, повторение шагов 1–4.
\end{enumerate}

\section{Результаты}
\begin{figure}
	\centering
	\includegraphics[width=0.5\textwidth]{img/fig2}
	\caption{\label{fig:2}}
\end{figure}

В рамках эксперимента были получены данные для шести различных масштабов:\footnote{Данные измерения не являются абсолютно точными, так как при обработке карт приходилось вручную удалять различные ненужные пометки, закрывающие береговую линию, которую потом приходилось дорисовывать вручную, поэтому данные, представленные в данном документе могут иметь погрешность.} (2,45, 1,25, 0,87, 0,645, 0,467 и 0,33) км/пкс. На графиках, представленных ниже (рис.~\ref{fig:1}), отображены результаты измерения длины береговой линии Греции в двойных логарифмических координатах. По оси $x$ отложены значения логарифма длин мерок в километрах, а по оси $y$~— логарифмы длины \abbr{БЛ}.

Согласно эмпирической формуле Ричардсона~\eqref{eq:2}, зависимость длины \abbr{БЛ} от длины мерки в двойных логарифмических координатах должна быть линейной. Однако, полученные кривые проявляют тенденцию к запределиванию при малых размерах мерки. Возможно, это связано с обилием небольших островов, в том числе с малой изрезанностью береговой линии, которые учитывались в расчётах.

\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{img/fig1}
	\caption{\label{fig:1}Зависимость длины береговой линии Греции от размера мерки при разных масштабах карты: (a)~$M=2.45$, (b)~$M=1.25$, (c)~$M=0.87$, (d)~$M=0.645$, (e)~$M=0.467$, (f)~$M=0.33$.}
\end{figure}

Для выявления влияния масштаба карты на длину береговых линий необходимо сравнить их длину при одинаковой длине мерки, но разных масштабах. В качестве такой мерки удобно взять мерку длиной 1\,км. Соответствующая длина береговой линии определяется по отрезку, отсекаемому кривыми (рис.~\ref{fig:1}) на оси $y$. Антилогарифм этой величины будет равен длине \abbr{БЛ} при мерке в 1\,км. Для того, чтобы рассчитать длину \abbr{БЛ} для каждого масштаба, необходимо возвести число 10 в степень, равную свободному члену.

По результатам расчетов длин береговой линии при мерке в 1\,км была составлена приведенная таблица~\ref{tab:2}. Исходя из данных таблицы, был построен график зависимости длины береговой линии при мерке в 1\,км от масштаба (рис.~\ref{fig:2}).

\begin{table}
	\centering
	\caption{\label{tab:2}Зависимость длины береговой линии от масштаба карты}
	\begin{tabular}{cccc}
		\hline
	 	№ & Масштаб карты & $\lg C$ & $L_\delta$ \\
	 	  & км/пкс &  & км \\
	 	\hline
	 	1 & 2,45 & 3,982 & 9594 \\
	 	2 & 1,25 & 4,038 & 10921 \\
	 	3 & 0,87 & 4,041 & 10992 \\
	 	4 & 0,645 & 4,171 & 14835 \\
	 	5 & 0,467 & 4,163 & 14561 \\
	 	6 & 0,33 & 4,231 & 17010 \\
	 	\hline
	\end{tabular}
\end{table}

Анализируя табличные данные и их графическое представление, можно заметить четкую тенденцию неограниченного увеличения длины береговой линии с уменьшением масштаба. Как следует из формулы, оно происходит по степенному закону.

\section{Основные результаты и выводы}
\begin{itemize}
	\item Проведя измерения длины \abbr{БЛ} Греции при семи разных масштабах, можно прийти к выводу, что при уменьшении масштаба карты длина береговой линии имеет тенденцию увеличиваться по степенному закону.
	\item Неограниченный рост длины береговой линии с уменьшением масштаба свидетельствует о том, что береговая линия~— это фрактал.
	\item Стоит отметить, что дальнейшее измерение длины \abbr{БЛ} при масштабах, меньших чем метр на пиксель, не имеет практического смысла, так как ее очертания, а соответственно и длина, постоянно меняется в течение времени под действием таких факторов как приливы, отливы и волны, которые со временем меняют форму берега. 
	\item Учитывая результаты исследования, следует подчеркнуть необходимость указания масштаба карты при публикации данных о длине береговых линий.
	\item В заключении хочется отметить, что береговые линии являются сложными системами, дальнейшее исследование которых еще предстоит впереди.
\end{itemize}

\section{Приложение}
Чтобы найти связь между фрактальной размерностью и коэффициентом $d$ в эмпирической формуле длины береговой линии, полученной Ричардсоном, рассмотрим длину береговой линии классического фрактала~— острова Коха, размерность которого известна. Попробуем получить формулу длины береговой линии острова Коха, содержащую его размерность в явном виде и сравнить её с эмпирическим уравнением Ричардсона.

Длина «береговой линии» острова Коха на $n$-ом шаге
\begin{equation}
	L_n=3\times\left(\frac43\right)^n,
	\label{eq:1}
\end{equation}
а длина $n$-ой мерки
\begin{equation}
	\delta_n=\left(\frac13\right)^n,
	\label{eq:2}
\end{equation}
Из уравнения~\eqref{eq:2} выразим $n$:
\begin{equation}
	n=-\frac{\lg \delta}{\lg 3}.
	\label{eq:3}
\end{equation}
Представим 4/3 в уравнении~\eqref{eq:1} в экспоненциальной форме, а вместо $n$ подставим его выражение из уравнения \eqref{eq:3}:
\begin{align}
	L_n &= 3\times\left(\frac43\right)^n=3\times 10^{n\lg 4/3} \nonumber\\
	&=3\times 10^{-\frac{\lg \delta_n}{\lg 3}(\lg 4-\lg 3)} \nonumber\\
	&=3\times 10^{\lg \delta_n(1-\lg 4/\lg 3)},
	\label{eq:4}
\end{align}
где $\lg 4/\lg 3$~— это известная нам по определению фрактальная размерность $D$ о.~Коха. Выразив $10^{\lg \delta_n}$ через $\delta_n$ и подставив вместо отношения $\lg 4/\lg 3$ его обозначение $D$, получим выражение для длины периметра острова Коха
\begin{equation}
	L_n=3\delta_n^{1-D},
	\label{eq:5}
\end{equation}
содержащее фрактальную размерность $D$ в явном виде. Эмпирическое уравнение зависимости $L_n$ от $\delta_n$, полученное Ричардсоном, имеет вид
\begin{equation}
	L_n=C\delta_n^{-d}.
	\label{eq:6}
\end{equation}
Принимая во внимание сходство уравнений~\eqref{eq:5} и~\eqref{eq:6}, можно записать, что
\begin{equation}
	D = 1+d.
	\label{eq:7}
\end{equation}
Таким образом, найдя значение $d$ из эксперимента по измерению длины береговой линии разными мерками, можно  по формуле~\eqref{eq:7} рассчитать фрактальную размерность береговой линии.

\section*{Библиография}
\small
\begin{biblist}
    \item Мандельброт~Б. Фрактальная геометрия природы. М.:\,Инст. Комп. Иссл. (2002).
    \item \textit{Fractalyse} \abbr{URL} \url{http://www.fractalyse.org/} [16.07.2018].
    \item Бинимелис Басса~М.\,И. Новый взгляд на мир. Фрактальная геометрия. М.:\,Де~Агостини (2004).
    \item Veritasium. What is the coastline paradox? \abbr{URL} \url{https://www.youtube.com/watch?v=I_rw-AJqpCM} [16.07.2018].
\end{biblist}


\end{document}
